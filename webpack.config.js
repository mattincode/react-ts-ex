import webpack from 'webpack';
import path from 'path';
import CopyPlugin from 'copy-webpack-plugin';

module.exports = {
  devtool: 'source-map',
  resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [".ts", ".tsx", ".js", ".json"]
  },
  entry: [
    'webpack-hot-middleware/client?reload=true',  //note that it reloads the page if hot module reloading fails.
    './src/index.tsx'
  ],
  devServer: {
    contentBase: './src'
  },
  target: 'web',
  output: {
    path: __dirname + '/dist',                  // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      noInfo: false,
      debug: true
    }),
    new CopyPlugin([
      {from: 'src/index.html', to: 'index.html'}
    ])
  ],
  module: {
    rules: [
      {test: /\.js$/, include: path.join(__dirname, 'src'), use: [{loader: "babel-loader"}]},
      {test: /\.tsx?$/, loader: "awesome-typescript-loader" },
      {enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
      {test: /(\.css)$/, include: path.join(__dirname, 'src'), use: [{loader:'style-loader'}, {loader:'css-loader'}]},
      {test: /(\.css)$/, exclude: path.join(__dirname, 'src'),  use: [{loader:'style-loader'}, {loader:'css-loader'}]},
      {test: /\.(woff|woff2)$/, use: [{loader:"url-loader", options: {prefix: "font", limit: 5000}}]},
      {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:'file-loader'}]},
      {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:"url-loader", options: {limit: 10000, mimeType: "application/octet-stream"}}]},
      {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, use: [{loader:"url-loader", options: {limit: 10000, mimeType: "image/svg+xml"}}]},
      {test: /\.(jpg|png|gif)$/, include: path.join(__dirname, 'src/images'), use: [{loader: "url-loader", options: {limit: 25000}}]}
    ]
  },
};
